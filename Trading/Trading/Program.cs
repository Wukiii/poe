﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Trading.Handlers;
using Trading.Objects;
using System.IO;
using System.Text;

namespace Trading {
    static class Program {
        private static Thread messageThread;
        private static string clientLogFilePath = "C:\\Program Files (x86)\\Grinding Gear Games\\Path of Exile\\logs\\client.txt";

        [STAThread]
        static void Main() {
            StartProgram();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }

        private static void StartProgram() {
            if (Program.messageThread != null) {
                Program.messageThread.Abort();
            }

            int numberOfStashTabs = 4;
            string currentItemsJSON = ""; // TOOD - Find JSON file with current items
            new ShopHandler(numberOfStashTabs, currentItemsJSON);
            new ControlHandler();
            new StateHandler();



            messageThread = new Thread(() => StateHandler.Instance.StartTradeThread());
            messageThread.Start();

            messageThread = new Thread(() => ControlHandler.Instance.StartControllerThread());
            messageThread.SetApartmentState(ApartmentState.STA);
            messageThread.Start();

            ListenOnLogFile(clientLogFilePath);
        }

        private static void ListenOnLogFile(string filePath) {
            TextHandler textHandler = new TextHandler();

            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader reader = new StreamReader(fs, Encoding.GetEncoding("UTF-8"));

            long offset = fs.Length;

            while (true) {
                fs.Seek(offset, SeekOrigin.Begin);
                if (!reader.EndOfStream) {
                    do {
                        string logMessage = reader.ReadLine();
                        Thread messageHandlerThread = new Thread(() => textHandler.HandleMessage(logMessage));
                        messageHandlerThread.Start();
                    }
                    while (!reader.EndOfStream);

                    offset = fs.Position;
                }

                    Thread.Sleep(1000);
            }
        }
    }
}
