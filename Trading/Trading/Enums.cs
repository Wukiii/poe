﻿namespace Trading {
    enum MessageType { Area, Trade, Whisper }
    enum ContainerType { Inventory, Stash, Trade }
    enum CurrencyType { chaos, exa, unknown }
}   