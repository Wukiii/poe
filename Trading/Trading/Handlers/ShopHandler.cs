﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Trading.Objects;

namespace Trading.Handlers {
    class ShopHandler {
        public static ShopHandler Instance;

        public int[,] Inventory { get; private set; }
        public List<int[,]> Stashes { get; private set; }
        private List<Item> _itemsInStore;
        private List<Currency> _currenciesInStore;

        private List<ListedCurrency> _listedCurrencies;

        private int _currentOpenStashTab;
        private UtilService _utilService;

        public ShopHandler(int numberOfStashTabs, string currentItemsJSON) {
            Instance = this;
            Inventory = new int[5, 12];
            Stashes = new List<int[,]>();
            for(int i = 0; i < numberOfStashTabs; i++) {
                Stashes.Add(new int[12, 12]);
            }
            _itemsInStore = new List<Item>();
            _currenciesInStore = new List<Currency>();
            _listedCurrencies = new List<ListedCurrency>();
            _currentOpenStashTab = 0;
            _utilService = new UtilService();

            // TODO - Load existing items from JSON file
            Item testExaltedItem = new Item("Exalted Orb", 1, 1);
            testExaltedItem.StashTab = 0;
            testExaltedItem.IsCurrency = true;
            testExaltedItem.CurrentPosition[0] = 2;
            testExaltedItem.CurrentPosition[1] = 4;
            _itemsInStore.Add(testExaltedItem);
            Stashes[testExaltedItem.StashTab][testExaltedItem.CurrentPosition[0] - 1, testExaltedItem.CurrentPosition[1] - 1] = 1;
            for (int i = 1; i < 15; i++) {
                Item testChaosItem = new Item("Chaos Orb", 1, 1);
                testChaosItem.StackSize = 10;
                testChaosItem.StashTab = 0;
                testChaosItem.IsCurrency = true;
                testChaosItem.CurrentPosition[0] = 1 + (i % 12);
                testChaosItem.CurrentPosition[1] = 1 + (i / 12);
                _itemsInStore.Add(testChaosItem);
                Stashes[testChaosItem.StashTab][testChaosItem.CurrentPosition[0] - 1, testChaosItem.CurrentPosition[1] - 1] = 1;
            }

            // TODO - Make public method for counting, adding and removing currencies stock
            Currency testExaltedCurrency = new Currency();
            testExaltedCurrency.Amount = 1;
            testExaltedCurrency.Name = "Exalted Orb";
            _currenciesInStore.Add(testExaltedCurrency);

            Currency testChaosCurrency = new Currency();
            testChaosCurrency.Amount = 140;
            testChaosCurrency.Name = "Chaos Orb";
            _currenciesInStore.Add(testChaosCurrency);

            // TODO - Make public method for enlisting currencies
            ListedCurrency testListedCurrency = new ListedCurrency();
            Currency testListedExaltedCurrency = new Currency();
            testListedExaltedCurrency.Amount = 1;
            testListedExaltedCurrency.Name = "Exalted Orb";
            testListedCurrency.Currency = testListedExaltedCurrency;

            Currency testListedExaltedPrice = new Currency();
            testListedExaltedPrice.Amount = 23;
            testListedExaltedPrice.Name = "Chaos Orb";
            testListedCurrency.Price = testListedExaltedPrice;

            _listedCurrencies.Add(testListedCurrency);

            ListedCurrency testListed2Currency = new ListedCurrency();
            Currency testListedChaosCurrency = new Currency();
            testListedChaosCurrency.Amount = 23;
            testListedChaosCurrency.Name = "Chaos Orb";
            testListed2Currency.Currency = testListedChaosCurrency;

            Currency testListedChaosPrice = new Currency();
            testListedChaosPrice.Amount = 1;
            testListedChaosPrice.Name = "Exalted Orb";
            testListed2Currency.Price = testListedChaosPrice;

            _listedCurrencies.Add(testListed2Currency);
        }

        public List<Item> ValidateAndFindCurrenciesInShop(Currency currencyToGive, Currency currencyToReceive) {
            // Check if the listing is correct
            ListedCurrency listedCurrency = _listedCurrencies.Find(lc => lc.Currency.Name.Equals(_utilService.MapCurrencyName(currencyToGive.Name)));

            Console.WriteLine("Stock ratio: " + listedCurrency.Currency.Amount / listedCurrency.Price.Amount);
            Console.WriteLine("Buyer ratio: " + currencyToGive.Amount / currencyToReceive.Amount);
            if (listedCurrency == null || 
                listedCurrency.Currency.Amount / listedCurrency.Price.Amount != currencyToGive.Amount / currencyToReceive.Amount) 
            {
                Console.WriteLine("No listed currency found or the price amount is not correct");
                return null;
            }

            // Check whenever the amount is in stock
            Currency currency = _currenciesInStore.Find(c => c.Name.Equals(_utilService.MapCurrencyName(currencyToGive.Name)));
            if (currency.Amount < currencyToGive.Amount) {
                Console.WriteLine("Not enough currencies in stock");
                return null;
            }

            List<Item> itemsToFind = new List<Item>();
            double amountLeftToFind = currencyToGive.Amount;
            foreach (Item item in _itemsInStore) {
                if (item.Name.Equals(_utilService.MapCurrencyName(currencyToGive.Name))) {

                    if (amountLeftToFind < item.StackSize) {
                        //Item newItem = new Item(item);
                        Item newItem = item.DeepClone<Item>();
                        newItem.StackSize = (int) amountLeftToFind;

                        // item.StackSize -= (int) amountLeftToFind;
                        itemsToFind.Add(newItem);

                        // TODO - If under 1 amount left to find, then find corresponding currencies.
                        //        Or don't, depending on if it makes sense.
                        return itemsToFind;
                    } else {
                        itemsToFind.Add(item);
                        amountLeftToFind -= item.StackSize;
                    }

                    if (amountLeftToFind == 0) {
                        return itemsToFind;
                    }
                }
            }

            Console.WriteLine("Seems currencies in store and items in store are not in sync");
            return null;
        }

        public Item IsItemInShop(string name, Currency price, int stashTab, int[] position) {
            foreach (Item item in _itemsInStore) {
                Console.WriteLine("Checking if it is item: " + item.Name);
                Console.WriteLine("Name: " + item.Name.Equals(name));
                Console.WriteLine("Price Amount: " + (item.Price.Amount == price.Amount));
                Console.WriteLine("Price Name: " + item.Price.Name.Equals(price.Name));
                Console.WriteLine("Stash tab: " + (item.StashTab == stashTab));
                Console.WriteLine("Start position: " + item.CurrentPosition.SequenceEqual(position));

                if (item.Name.Equals(name) &&
                    item.Price.Amount == price.Amount &&
                    item.Price.Name.Equals(price.Name) &&
                    item.StashTab == stashTab &&
                    item.CurrentPosition.SequenceEqual(position)) {
                    Console.WriteLine("Item exist in shop");
                    return item;
                }
            }
            Console.WriteLine("Item does not exist in shop");
            return null;
        }

        public void AddItemToInventoryArray(Item item, Action callback = null) {
            if(IsRoomForItemInArray(item, Inventory, true, true)) {
                item.InInventory = true;
            } else {
                Console.WriteLine("No room in inventory, shouldn't be possible to trade then");
            }

            callback?.Invoke();
        }

        public void AddItemToShop(Item item) {
            if (item.IsCurrency) {
                Currency currencyToAddTo = _currenciesInStore.Find(i => i.Name.Equals(_utilService.MapCurrencyName(item.Name)));
                currencyToAddTo.Amount += item.StackSize;
            }

            foreach (Item shopItem in _itemsInStore) {
                if (shopItem.InInventory && shopItem.Name.Equals(item.Name)) {
                    if (shopItem.CurrentPosition[0] == item.CurrentPosition[0] &&
                        shopItem.CurrentPosition[1] == item.CurrentPosition[1]) {
                        shopItem.StackSize += item.StackSize;
                    }
                    return;
                } 
            }

            _itemsInStore.Add(item);
        }

        public void AddInventoryToShop(Action callback) {
            
        }

        public void RemoveItemsFromShop(List<Item> items) {
           foreach (Item item in items) {
                if (item.InInventory) {
                    RemoveItemFromArray(item, item.CurrentPosition[0] - 1, item.CurrentPosition[1] - 1, Inventory);
                    Item itemToRemove = _itemsInStore.Find(i => i == item);

                    _itemsInStore.Remove(item);



                    if (item.IsCurrency) {
                        Currency currencyToRemoveFrom = _currenciesInStore.Find(i => i.Name.Equals(_utilService.MapCurrencyName(item.Name)));
                        currencyToRemoveFrom.Amount -= item.StackSize;
                    }
                } else {
                    Console.WriteLine("TODO - Handle if removing items from shop in stash");
                }
            }
        }

        public void AddItemToStashesArray(Item item) {
            bool isRoom = IsRoomForItemInArray(item, Stashes[item.StashTab], true, true, true);
            if (isRoom) {
                item.InInventory = false;
            } else {
                Console.WriteLine("No room in stash, convert currencies.");
            }
        }

        public void PutItemToStash(Item item, int[] position) {
            int stashTabOffset = item.StashTab - _currentOpenStashTab;

            if (stashTabOffset != 0) {
                _currentOpenStashTab = item.StashTab;
            }

            RemoveItemFromArray(item, item.CurrentPosition[0] - 1, item.CurrentPosition[1] - 1, Inventory);

            ControlHandler.Instance.AddToActionQueue(
                () => ControlHandler.Instance.TransferItem(ContainerType.Inventory, position, stashTabOffset));
        }

        public void PutItemsToStash(List<Item> items) {
            for (int i = 0; i < items.Count; i++) {
                PutItemToStash(items[0], new int[2] { 1 + (i % 12), 1 + (i / 12) });
            }


            //foreach (Item item in items) {
            //    PutItemToStash(item);
            //}
        }

        public void GetItemFromStash(Item item, Action callback) { 
            // Changing stash tab if necessary
            int stashTabOffset = item.StashTab - _currentOpenStashTab;

            if (stashTabOffset != 0) {
                _currentOpenStashTab = item.StashTab;
            }

            Item itemToSplit = _itemsInStore.Find(i => {
                return i.CurrentPosition[0] == item.CurrentPosition[0] && 
                       i.CurrentPosition[1] == item.CurrentPosition[1];
            });
            int splitAmount = itemToSplit.StackSize == item.StackSize ? 0 : item.StackSize;

            Console.WriteLine("Sending TransferItem action for stash");
            Console.WriteLine("callback: " + callback);
            if (splitAmount != 0) {
                AddItemToInventoryArray(item);
                itemToSplit.StackSize -= splitAmount;
                SplitAmounrProperties splitAmountProp = new SplitAmounrProperties(splitAmount, ContainerType.Inventory, item.CurrentPosition);

                // Updating items in store
                _itemsInStore.Add(item);

                ControlHandler.Instance.AddToActionQueue(
                () => ControlHandler.Instance.TransferItem(ContainerType.Stash, itemToSplit.CurrentPosition, stashTabOffset, splitAmountProp),
                callback);
            } else {
                // Updateing stashes array
                RemoveItemFromArray(item, item.CurrentPosition[0] - 1, item.CurrentPosition[1] - 1, Stashes[item.StashTab]);

                ControlHandler.Instance.AddToActionQueue(
                () => ControlHandler.Instance.TransferItem(ContainerType.Stash, item.CurrentPosition, stashTabOffset),
                () => AddItemToInventoryArray(item, callback));
            }
        }

        public void GetItemsFromStash(List<Item> items, Action callback) {
            if (items.Count > 1) {
                GetItemFromStash(items[0], () => GetItemsFromStash(items.GetRange(1, items.Count - 1), callback));
            } else {
                GetItemFromStash(items[0], callback);
            }
        }

        public bool IsRoomForItemsInInventory(List<Item> items) {
            int[,] arrayToInsert = (int[,]) Inventory.Clone();
            foreach (Item item in items) {
                if (!IsRoomForItemInArray(item, arrayToInsert, true, false)) {
                    return false;
                }
            }
            return true;
        }

        public bool IsRoomForItemsInStashes(List<Item> items) {
            List<int[,]> stashToInsertInto = Stashes.GetRange(0, Stashes.Count);

            bool foundRoomForItems = true;
            foreach (Item item in items) {
                int stashTabIndex = 0;
                foundRoomForItems = false;
                foreach (int[,] stash in stashToInsertInto) {
                    if (IsRoomForItemInArray(item, stash, true, false, true)) {
                        item.StashTab = stashTabIndex;
                        foundRoomForItems = true;
                        break;
                    }
                    stashTabIndex++;
                }
            }
            
            return foundRoomForItems;
        }

        public bool IsRoomForItemInArray(
            Item item,
            int[,] arrayToInsert,
            bool addToArray = false,
            bool changeItems = true,
            bool compareWithStash = false) 
        {
            for (int xPosition = 0; xPosition < arrayToInsert.GetLength(1); xPosition++) {
                for (int yPosition = 0; yPosition < arrayToInsert.GetLength(0); yPosition++) {
                    bool foundRoom = true;
                    for (int i = 0; i < item.Width; i++) {
                        if (xPosition + i >= arrayToInsert.GetLength(1)) {
                            return false;
                        }

                        for (int j = 0; j < item.Height; j++) {
                            if (yPosition + j >= arrayToInsert.GetLength(0)) {
                                foundRoom = false;
                                break;
                            }

                            if (arrayToInsert[yPosition + j, xPosition + i] != 0) {
                                //if (item.IsCurrency) {
                                //    Item itemInShop = _itemsInStore.Find(it => {
                                //        return it.CurrentPosition[0] == yPosition + j && 
                                //               it.CurrentPosition[1] == xPosition + i &&
                                //               compareWithInventory ? it.InInventory : !it.InInventory;
                                //    });

                                //    if (itemInShop.IsCurrency && itemInShop.Name.Equals(item.Name)) {
                                //        // TODO - Check if currencies will/can be merged

                                //    }

                                //}

                                foundRoom = false;
                                break;
                            } 
                        }
                        if (!foundRoom) {
                            break;
                        }
                    }
                    if (foundRoom) {
                        if (addToArray) {
                            InsertItemInIntoArray(item, yPosition, xPosition, arrayToInsert);
                            if (changeItems) {
                                item.CurrentPosition[0] = yPosition + 1;
                                item.CurrentPosition[1] = xPosition + 1;
                            }
                        }
                        return true;
                    }
                }
            }

            Console.WriteLine("No room for item");
            return false;
        }

        // Only handling the first stash tab
        // TODO - Make it able to do all stash tabs later
        public bool HandleRoomForCurrenciesInStashArray(List<Item> items) {
            List<Item> tempItems = items.DeepClone();
            List<Item> tempItemInStore = _itemsInStore.DeepClone();
            List<Currency> tempCurrenciesInStore = _currenciesInStore.DeepClone(); ;
            List<int[,]> tempStash = Stashes.DeepClone();

            foreach (Item item in items) {
                tempCurrenciesInStore.Find(c => c.Name == item.Name).Amount += item.StackSize;
            }

            List<Item> itemsWithNoFullStack = tempItems.FindAll(i =>
                i.StackSize < _utilService.CurrencyProperties[i.Name].MaxStackSize
            );

            List<Item> stashItemsWithNoFullStack = tempItemInStore.FindAll(i => {
                return i.IsCurrency &&
                       i.StackSize < _utilService.CurrencyProperties[i.Name].MaxStackSize;
            });

            foreach (Item inventoryItem in itemsWithNoFullStack) {
                int maxStackSize = _utilService.CurrencyProperties[inventoryItem.Name].MaxStackSize;
                foreach (Item stashItem in stashItemsWithNoFullStack) {
                    if (stashItem.StashTab == 0 && stashItem.Name.Equals(inventoryItem.Name)) {
                        int newStackSize = stashItem.StackSize + inventoryItem.StackSize;
                        if (newStackSize <= maxStackSize) {
                            tempItems.Remove(inventoryItem);
                            stashItem.StackSize = newStackSize;
                            break;
                        } else {
                            inventoryItem.StackSize = newStackSize - maxStackSize;
                            stashItem.StackSize = maxStackSize;
                        }
                    }
                }
            }


            if (tempItems.Count > 0) {
                for (int i = 0; i < 12; i++) {
                    for (int j = 0; j < 12; j++) {
                        if (tempStash[0][j, i] == 0) {
                            tempStash[0][j, i] = 1;
                            tempItems[0].CurrentPosition[0] = j + 1;
                            tempItems[0].CurrentPosition[1] = i + 1;

                            tempItemInStore.Add(tempItems[0]);
                            tempItems.RemoveAt(0);

                            if (tempItems.Count == 0) {
                                _itemsInStore = tempItemInStore;
                                _currenciesInStore = tempCurrenciesInStore;
                                Stashes = tempStash;
                                return true;
                            }
                        }
                    }
                }
            } else {
                _itemsInStore = tempItemInStore;
                _currenciesInStore = tempCurrenciesInStore;
                Stashes = tempStash;
                return true;
            }


                return false;
        }

        private void InsertItemInIntoArray(Item item, int yPosition, int xPosition, int[,] arrayToInsert) {
            Console.WriteLine("Inserting item into array");
            for (int i = 0; i < item.Width; i++) {
                for (int j = 0; j < item.Height; j++) {
                    arrayToInsert[yPosition + j, xPosition + i] = 1;

                    //Console.WriteLine("Filling position: " + (yPosition + j) + " " + (xPosition + i));
                }
            }
        }

        private void RemoveItemFromArray(Item item, int yPosition, int xPosition, int[,] arrayToRemoveFrom) {
            Console.WriteLine("Removing item from array");
            for (int i = 0; i < item.Width; i++) {
                for (int j = 0; j < item.Height; j++) {
                    arrayToRemoveFrom[yPosition + j, xPosition + i] = 0;

                    //Console.WriteLine("Filling position: " + (yPosition + j) + " " + (xPosition + i));
                }
            }
        }
    }
}
