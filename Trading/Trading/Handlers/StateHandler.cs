﻿using System;
using System.Collections.Generic;
using System.Threading;
using Trading.Objects;

namespace Trading.Handlers {
    class StateHandler {
        public static StateHandler Instance;
        private List<PartyEntry> _currentParty, _inviteQueue, _tradeQueue;

        private ControlHandler _controlHandler;
        private ShopHandler _shopHandler;
        private UtilService _utilService;

        public PartyEntry CurrentTradingPartyEntry { get; set; }
        public List<Currency> CountedCurrencies { get; set; }
        public List<Item> ItemsInCountingContainer { get; set; }
        public List<Item> ItemsInGivingTradeWindow { get; set; }

        // Trading windows variables
        private int _currencyCountedTimes;
        private double _lastCountedValue;
        private bool _passedFirstValidation;

        public StateHandler() {
            Instance = this;
            _currentParty = new List<PartyEntry>();
            _inviteQueue = new List<PartyEntry>();
            _tradeQueue = new List<PartyEntry>();
            _controlHandler = ControlHandler.Instance;
            _shopHandler = ShopHandler.Instance;
            _utilService = new UtilService();
            CountedCurrencies = new List<Currency>();
            ItemsInCountingContainer = new List<Item>();
            ItemsInGivingTradeWindow = new List<Item>();
    }

    public void StartTradeThread() {
            while (true) {
                //Console.WriteLine("Invite queue size: " + _inviteQueue.Count + " - Trading queue size is: " + _tradeQueue.Count);

                if (_inviteQueue.Count > 0) {
                    Console.WriteLine("Invite queue size is: " + _inviteQueue.Count);
                    if (_currentParty.Count < 5) {
                        Console.WriteLine("Inviting: " + _inviteQueue[0].Message.CharacterName);
                        InvitePlayer(_inviteQueue[0]);
                        _inviteQueue.RemoveAt(0);
                    } else {
                        // TODO - Check whenever any existing party members can be kicked.
                        //        Else write a fine wait message

                        // If someone get kicked, invite next player in line
                        //InvitePlayer(_tradeQueue[0]);
                    }
                }

                if (_tradeQueue.Count > 0) {
                    Console.WriteLine("Trading queue size is: " + _tradeQueue.Count);
                    if (CurrentTradingPartyEntry == null) {
                        Console.WriteLine("Starting trade flow with: " + _tradeQueue[0].Message.CharacterName);
                        CurrentTradingPartyEntry = _tradeQueue[0];
                        _tradeQueue.RemoveAt(0);
                        Thread tradeThread = new Thread(() => StartTradingFlow());
                        tradeThread.Start();
                    }
                }

                Thread.Sleep(1000);
            }
        }

        public void AddToInviteQueue(PartyEntry partyEntry) {
            _inviteQueue.Add(partyEntry);
        }

        public bool ExistInParty(string characterName) {
            bool inQueue = _inviteQueue.Find(entry => entry.Message.CharacterName.Equals(characterName)) != null;
            bool inParty = _currentParty.Find(entry => entry.Message.CharacterName.Equals(characterName)) != null;

            return inQueue || inParty;
        }

        public void AddToTradeQueue(PartyEntry partyEntry) {
            _tradeQueue.Add(partyEntry);
        }

        public void InvitePlayer(PartyEntry partyEntry) {
            Console.WriteLine("Sending InviteCharacter action");
            _controlHandler.AddToActionQueue(() => _controlHandler.InviteCharacter(partyEntry.Message.CharacterName));
            _currentParty.Add(partyEntry);

            string messageToSend;

            if (partyEntry.ItemToSell != null) {
                messageToSend = "@" + partyEntry.Message.CharacterName +
                                " Your item - " + partyEntry.ItemToSell.Name + " - for " +
                                partyEntry.ItemToSell.Price.Amount + " " + partyEntry.ItemToSell.Price.Name +
                                " is ready to be picked up :{)}";
            } else {

                messageToSend = "@" + partyEntry.Message.CharacterName +
                                " My " + partyEntry.CurrencyAmountTradeMessage + " for " + 
                                partyEntry.CurrencyPriceTradeMessage + " is ready to be picked up :{)}";
            }

            _controlHandler.AddToActionQueue(() => _controlHandler.SendChatMessage(messageToSend));

            // TODO - Temporary reserve item, so other can't 'buy' the same item.
            //        Find a way to reverse it if they lay the invite hanging or dont accept it.
        }

        public void PlayerJoineOrLeftArea(string name, bool inArea) {
            PartyEntry partyEntry = _currentParty.Find(entry => entry.Message.CharacterName.Equals(name));
            if (partyEntry != null) {
                partyEntry.IsInTheArea = inArea;

                if (inArea) {
                    AddToTradeQueue(partyEntry);
                } else {
                    // TODO - Put items back if trade failed, or kick player if trade accepted
                }
            }
        }

        public void StartTradingFlow() {
            //Console.WriteLine("Trading with player: " + name);
            //PartyEntry partyEntry = _currentParty.Find(x => x.Message.CharacterName.Equals(name));
            PartyEntry partyEntry = CurrentTradingPartyEntry;

            if (!partyEntry.TradeDone) {
                // TODO - Check whenever the item is in stash or inventory
                if (partyEntry.CurrenciesToSell != null) {
                    if (!_shopHandler.IsRoomForItemsInInventory(partyEntry.CurrenciesToSell)) {
                        Console.WriteLine("TODO - Make room for items or trade multiple times");
                        return;
                    }
                    _shopHandler.GetItemsFromStash(partyEntry.CurrenciesToSell, () => TradeWithPlayer(partyEntry));
                } else if (_shopHandler.IsRoomForItemInArray(partyEntry.ItemToSell, _shopHandler.Inventory)) {
                    Console.WriteLine("Sending TradeWithPlayer action");
                    _shopHandler.GetItemFromStash(partyEntry.ItemToSell, () => TradeWithPlayer(partyEntry));
                } else {
                    Console.WriteLine("TODO - Handle no room in inventory - Not likly to happen unless concurrency problem or errors in code.");
                }
            } else {
                RemoveFirstFromTrade();
            }
        }
        private void RemoveFirstFromTrade() {
            CurrentTradingPartyEntry = null;
        }

        public void TradeWithPlayer(PartyEntry partyEntry) {
            _currencyCountedTimes = 0;
            // Trade with the player
            Console.WriteLine("Sending TradeWithCharacter action");
            _controlHandler.AddToActionQueue(() => _controlHandler.RequestTradeWithCharacter(partyEntry.Message.CharacterName), CheckIfTrading);
        }

        public void CheckIfTrading() {
            // TODO - Find out if there is a better way to check if a use accept the trading or leave it hanging 
            Thread.Sleep(6000);
            Console.WriteLine("Sending TryCancelTrade action");
            _controlHandler.AddToActionQueue(() => _controlHandler.TryCancelTrade(), () => CheckIfTradeStarted());       
        }

        public void CheckIfTradeStarted() {
            if (CurrentTradingPartyEntry.RejectedTrade) {
                Console.WriteLine("TODO - Handle rejected trade");
                // TODO - Try again, but give less time to accept

            } else {
                Console.WriteLine("TODO - Handle trade");
                PartyEntry partyEntry = CurrentTradingPartyEntry;

                double valueOfITemToSell = 0;
                Console.WriteLine("Sending TransferItem action for inventory");
                ItemsInGivingTradeWindow.Clear();
                if (partyEntry.ItemToSell != null) {
                    _controlHandler.AddToActionQueue(() => _controlHandler.TransferItem(ContainerType.Inventory, partyEntry.ItemToSell.CurrentPosition));
                    ItemsInGivingTradeWindow.Add(partyEntry.ItemToSell);

                    valueOfITemToSell = _utilService.GetValue(partyEntry.ItemToSell.Price);
                } else {
                    Currency currenciesToSell = new Currency() { 
                        Name = partyEntry.CurrencyAmountTradeMessage.Split(' ')[1],
                        Amount = double.Parse(partyEntry.CurrencyAmountTradeMessage.Split(' ')[0])
                    };

                    valueOfITemToSell = _utilService.GetValue(currenciesToSell);
                    foreach (Item item in partyEntry.CurrenciesToSell) {
                        _controlHandler.AddToActionQueue(() => _controlHandler.TransferItem(ContainerType.Inventory, item.CurrentPosition));

                        ItemsInGivingTradeWindow.Add(item);
                    }
                }
                Console.WriteLine("Sending CountTradeWindow action");
                _controlHandler.AddToActionQueue(() => _controlHandler.CountCurrencies(ContainerType.Trade), () => ValidateCountedCurrencies(valueOfITemToSell));
            }
        }

        public void ValidateCountedCurrencies(double valueOfITemToSell) {
            //Validating receiving currencies
            double valueCounter = 0;
            foreach (Currency currency in CountedCurrencies) {
                valueCounter += _utilService.GetValue(currency);
                if (valueCounter == 0) {
                    // TODO - unknown currency, accept currency and log
                    Console.WriteLine("Uknown currency: " + currency.Name);
                } else {
                    Console.WriteLine(currency.Name);
                    Console.WriteLine("Got value: " + valueCounter);
                }
            }

            if (_lastCountedValue != valueCounter) {
                // Changes in currency input -> Resetting counter
                _currencyCountedTimes = 0;
            }

            _lastCountedValue = valueCounter;

            if (valueOfITemToSell == valueCounter) {
                if (_passedFirstValidation) {
                    Console.WriteLine("Passed first validation");
                    // TODO - Accept trade
                    Console.WriteLine("Accepting the trade");

                    _controlHandler.AddToActionQueue(() => _controlHandler.AcceptTrade());

                    // TODO - Missing handling for is buyer leaves the trade window hanging or rejecting
                    //        Maybe handle with a callback?
                    return;
                    Thread.Sleep(5000);

                    if(CurrentTradingPartyEntry.TradeDone) {
                        // Stash items
                        return;
                    }

                    if (CurrentTradingPartyEntry.RejectedTrade) {
                        Console.WriteLine("TODO - Handle trade rejected?");
                      
                    }

                } else {
                    _passedFirstValidation = true;
                    _controlHandler.AddToActionQueue(() => _controlHandler.CountCurrencies(ContainerType.Trade), () => ValidateCountedCurrencies(valueOfITemToSell));

                }
                return;
            } else {
                Console.WriteLine("TODO - Handle case when not correct value");
                Console.WriteLine("Got: " + valueCounter + " but need " + valueOfITemToSell);
                _passedFirstValidation = false;
                _currencyCountedTimes += 1;
                Console.WriteLine("Counted " + _currencyCountedTimes + " times.");
            }

            if (CurrentTradingPartyEntry.RejectedTrade || _currencyCountedTimes > 5) {
                Console.WriteLine("TODO - Handle rejected trade");
                return;
            }

            CountedCurrencies = new List<Currency>();
            Console.WriteLine("Sending CountTradeWindow action");
            _controlHandler.AddToActionQueue(() => _controlHandler.CountCurrencies(ContainerType.Trade), () => ValidateCountedCurrencies(valueOfITemToSell));

        }

        public void FinishTrade() {
            CurrentTradingPartyEntry.TradeDone = true;

            _shopHandler.RemoveItemsFromShop(ItemsInGivingTradeWindow);



            //// Adding items to shop
            //foreach (Item item in ItemsInReceivingTradeWindow) {
            //    // TODO - Do over! -> Make is traverse the inventory 
            //    //        and check for what you need to add
            //    _shopHandler.AddItemToInventoryArray(item);
            //    _shopHandler.AddItemToShop(item);
            //}

            string messageToSend = "@" + CurrentTradingPartyEntry.Message.CharacterName +
                                " Thank you for the trade :{)} Have a nice day and may the RNGesus always be with you!";

            _controlHandler.AddToActionQueue(() => _controlHandler.SendChatMessage(messageToSend));
            _controlHandler.AddToActionQueue(() => _controlHandler.KickCharacter(CurrentTradingPartyEntry.Message.CharacterName));
            _controlHandler.AddToActionQueue(() => _controlHandler.OpenStash());
            _controlHandler.AddToActionQueue(() => _controlHandler.CountCurrencies(ContainerType.Inventory), () => InsertItemsInStash());
        }

        public void InsertItemsInStash() {
            if (_shopHandler.HandleRoomForCurrenciesInStashArray(ItemsInCountingContainer)) {
                _shopHandler.PutItemsToStash(ItemsInCountingContainer);
            } else {
                Console.WriteLine("TODO - No room in stash. Stop bot and convert");
            }



            //if (!_shopHandler.IsRoomForItemsInStashes(ItemsInCountingContainer)) {
            //    Console.WriteLine("TODO - No room in stash. Stop bot and convert");
            //} else {
            //    _shopHandler.PutItemsToStash(ItemsInCountingContainer);
            //}

            CleanUpFromTrade();
        }

        private void CleanUpFromTrade() {
            CountedCurrencies.Clear();
            ItemsInGivingTradeWindow.Clear();
            ItemsInCountingContainer.Clear();
            _passedFirstValidation = false;
            _lastCountedValue = 0;

            _currentParty.Remove(CurrentTradingPartyEntry);
            CurrentTradingPartyEntry = null;
        }
    }
}
