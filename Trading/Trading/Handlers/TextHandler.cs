﻿using System;
using Trading.Objects;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Collections.Generic;

namespace Trading.Handlers {
    class TextHandler {
        public void HandleMessage(string logText) {
            Message message = new Message(logText);

            if (!message.ignoreMessage) {
                if (message.TypeOFMessage == MessageType.Area) {
                    StateHandler.Instance.PlayerJoineOrLeftArea(message.CharacterName, message.IsJoinedArea);
                    return;
                }
                if (message.TypeOFMessage == MessageType.Whisper) {
                    try {
                        string[] array = Regex.Split(message.MessageText, " listed for ");
                        // Normal item trade text
                        if (array.Length > 1) {
                            // Getting item name
                            string itemNameToSell = array[0].Substring(29);

                            // Getting price
                            int endIndexForPrice = array[1].IndexOf(" in Delirium ");
                            string[] priceArray = array[1].Substring(0, endIndexForPrice).Split(' ');
                            if (priceArray.Length != 2) {
                                throw new Exception("Price is not correct");
                            }
                            Currency price = new Currency();
                            Console.WriteLine(priceArray[0]);
                            price.Amount = double.Parse(priceArray[0], CultureInfo.InvariantCulture);
                            price.Name = priceArray[1];

                            // Getting stash tab name
                            int stashTabNameStartIndex = array[1].IndexOf("\"") + 1;
                            int stashTabNameEndIndex = array[1].IndexOf("\"", array[1].IndexOf('\"') + 1);
                            int stashTab = int.Parse(array[1].Substring(stashTabNameStartIndex, stashTabNameEndIndex - stashTabNameStartIndex));

                            // Getting item position
                            string stringToSplit = array[1].Substring(array[1].IndexOf(" left ") + 6);
                            string[] position = Regex.Split(stringToSplit, ", top ");
                            position[1] = position[1].Substring(0, position[1].Length - 1);
                            
                            Item itemToFind = ShopHandler.Instance.IsItemInShop(itemNameToSell, price, stashTab, Array.ConvertAll(position, int.Parse));

                            if (itemToFind == null) {
                                string messageToSend = "@" + message.CharacterName + " The item - " + itemNameToSell + " - for " + price.Amount + " " + price.Name + " is sold :{(}";
                                ControlHandler.Instance.AddToActionQueue(() => ControlHandler.Instance.SendChatMessage(messageToSend));
                                return;
                            } 

                            PartyEntry partyEntry = new PartyEntry(message, itemToFind);
                            StateHandler.Instance.AddToInviteQueue(partyEntry);
                            return;
                        }
                        // Currency shop text
                        if (!StateHandler.Instance.ExistInParty(message.CharacterName) &&
                            message.MessageText.Contains(" like to buy your ")) 
                        {
                            array = Regex.Split(message.MessageText, " for my ");

                            // Getting currency to give
                            string currencyToGiveMessage = Regex.Split(array[0], " like to buy your ")[1];
                            string[] priceGive = currencyToGiveMessage.Split(' ');
                            Currency currencyToGive = new Currency();
                            currencyToGive.Amount = double.Parse(priceGive[0]);
                            currencyToGive.Name = priceGive[1];

                            // Getting currency to receive
                            string currencyToReceiveMessage = array[1].Substring(0, array[1].IndexOf(" in Delirium"));
                            string[] priceReceive = currencyToReceiveMessage.Split(' ');
                            Currency currencyToReceive = new Currency();
                            currencyToReceive.Amount = double.Parse(priceReceive[0]);
                            currencyToReceive.Name = priceReceive[1];

                            Console.WriteLine(currencyToGive.Amount);
                            Console.WriteLine(currencyToGive.Name);
                            Console.WriteLine(currencyToReceive.Amount);
                            Console.WriteLine(currencyToReceive.Name);

                            List<Item> currenciesToFind = ShopHandler.Instance.ValidateAndFindCurrenciesInShop(currencyToGive, currencyToReceive);

                            if (currenciesToFind == null) {
                                string messageToSend = "@" + message.CharacterName + " Sry, but I am out of " + currencyToGive.Name + " for now.";
                                ControlHandler.Instance.AddToActionQueue(() => ControlHandler.Instance.SendChatMessage(messageToSend));
                                return;
                            }


                            PartyEntry partyEntry = new PartyEntry(message, currenciesToFind, currencyToGiveMessage, currencyToReceiveMessage);
                            StateHandler.Instance.AddToInviteQueue(partyEntry);
                            return;
                        }



                    } catch (Exception e) {
                        Console.WriteLine(e);
                        Console.WriteLine("TODO - Log error and send a nice decline answer to the buyer");
                    }
                    

                    Console.WriteLine(message.CharacterName);
                    Console.WriteLine(message.Date);
                    Console.WriteLine(message.ShortGuildName);
                    Console.WriteLine(message.MessageText);

                    return;
                }
            }
        }
    }
}
