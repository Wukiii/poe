﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Trading.Objects;

namespace Trading.Handlers {
    class ControlHandler {
        public static ControlHandler Instance;

        [DllImport("User32.dll")]
        private static extern bool SetCursorPos(int X, int Y);
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int VK_LCONTROL = 0xA2; //Left Control key code
        private const int VK_LSHIFT = 0xA0; //Left Shift key code
        private const int KEYEVENTF_KEYDOWN = 0x0000; // New definition
        private const int KEYEVENTF_EXTENDEDKEY = 0x0001; //Key down flag
        private const int KEYEVENTF_KEYUP = 0x0002; //Key up flag
        private const int C = 0x43; //C key code

        // Variables for counting
        private List<Currency> _currencyList;
        private ContainerType _containerToCountCurrenciesIn;
        private int[] _currenctCountingPosition;

        private List<Action[]> _actionQueue;

        public ControlHandler() {
            Instance = this;
            _actionQueue = new List<Action[]>();
            _currencyList = new List<Currency>();
            ClipboardMonitor.OnClipboardChange += ClipboardChanged;
            ClipboardMonitor.Start();
        }

        public void StartControllerThread() {
            while (true) {
                if (_actionQueue.Count > 0) {
                    Console.WriteLine("Action queue size is: " + _actionQueue.Count);
                    Console.WriteLine("Executing action: " + _actionQueue[0][0].Method);
                    _actionQueue[0][0].Invoke();
                    _actionQueue[0][1]?.Invoke();
                    _actionQueue.RemoveAt(0);
                }

                Thread.Sleep(100);
            }
        }

        public void AddToActionQueue(Action action, Action callback = null) {
            _actionQueue.Add(new Action[2] { action, callback });
        }

        public void MoveMouse(ContainerType containerToMoveIn, double yNumberBlock, double xNumberBlock) {
            double blockSize = 52.64;
            double xStartPosition = 0;
            double yStartPosition = 0;

            switch (containerToMoveIn) {
                case ContainerType.Inventory:
                    xStartPosition = 1270 + blockSize / 2;
                    yStartPosition = 586 + blockSize / 2;
                    break;
                case ContainerType.Stash:
                    xStartPosition = 15 + blockSize / 2;
                    yStartPosition = 160 + blockSize / 2;
                    break;
                case ContainerType.Trade:
                    xStartPosition = 310 + blockSize / 2;
                    yStartPosition = 203 + blockSize / 2;
                    break;
            }

            double xBlockOffset = blockSize * (xNumberBlock - 1);
            double yBlockOffset = blockSize * (yNumberBlock - 1);

            SetCursorPos((int) (xStartPosition + xBlockOffset), (int) (yStartPosition + yBlockOffset));
            Thread.Sleep(10);
        }

        public void SendChatMessage(string message) {
            Console.WriteLine("Sending message: " + message);
            SendKeys.SendWait("{ENTER}");
            SendKeys.SendWait(message);
            SendKeys.SendWait("{ENTER}");
        }

        public void InviteCharacter(string name) {
            Console.WriteLine("Inviting " + name + " to party");
            SendKeys.SendWait("{ENTER}");
            SendKeys.SendWait("/invite " + name);
            SendKeys.SendWait("{ENTER}");
        }

        public void KickCharacter(string name) {
            Console.WriteLine("Kicking " + name + " from party");
            SendKeys.SendWait("{ENTER}");
            SendKeys.SendWait("/kick " + name);
            SendKeys.SendWait("{ENTER}");
        }

        public void RequestTradeWithCharacter(string name) {
            Console.WriteLine("Sending trading request to " + name);
            SendKeys.SendWait("{ENTER}");
            SendKeys.SendWait("/tradewith " + name);
            SendKeys.SendWait("{ENTER}");
        }

        public void AcceptTrade() {
            ControlHandler.Instance.MoveMouse(ContainerType.Trade, 12.5, 1);
            Thread.Sleep(100);
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        }

        public void ChangeStashTab(int stashTabOffset) {
            string leftOrRight = stashTabOffset < 0 ? "{LEFT " : "{RIGHT ";
                
            SendKeys.SendWait(leftOrRight + Math.Abs(stashTabOffset) + "}");
        }

        public void TransferItem(ContainerType containerToMoveFrom, int[] position, int stashTabOffset = 0, SplitAmounrProperties splitAmount = null) {
            Console.WriteLine("Tranfering Item");
            if (stashTabOffset != 0) {
                ChangeStashTab(stashTabOffset);
            }

            MoveMouse(containerToMoveFrom, position[0], position[1]);
            Thread.Sleep(40);
            if (splitAmount != null) {
                keybd_event(VK_LSHIFT, 0, KEYEVENTF_KEYDOWN, 0);
                Thread.Sleep(5);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                Thread.Sleep(5);
                mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                Thread.Sleep(5);
                keybd_event(VK_LSHIFT, 0, KEYEVENTF_KEYUP, 0);
                Thread.Sleep(5);
                SendKeys.SendWait(splitAmount.AmountToSplit + "{ENTER}");
                MoveMouse(splitAmount.ContainerToTransferTo, splitAmount.Position[0], splitAmount.Position[1]);
                Thread.Sleep(500);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                Thread.Sleep(10);
                mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                Thread.Sleep(40);
            } else {
                keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYDOWN, 0);
                Thread.Sleep(5);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                Thread.Sleep(5);
                mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                Thread.Sleep(5);
                keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYUP, 0);
                Thread.Sleep(40);
            }
        }

        public void TryCancelTrade() {
            MoveMouse(ContainerType.Trade, 7, 16);
            Thread.Sleep(100);
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);

            Thread.Sleep(1500);
        }

        public void OpenStash() {
            Thread.Sleep(1000);
            MoveMouse(ContainerType.Trade, 7, 16);
            Thread.Sleep(100);
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);

            Thread.Sleep(1000);
        }

        // Traverse through the container and counting the currencies.
        // Only supporting Trade and Inventory atm.
        public void CountCurrencies(ContainerType containerToCountIn) {
            Console.WriteLine("Counting trade window");
            _currencyList = new List<Currency>();
            _containerToCountCurrenciesIn = containerToCountIn;
            StateHandler.Instance.ItemsInCountingContainer = new List<Item>();

            for (int i = 1; i <= 12; i++) {
                for (int j = 1; j <= 5; j++) {
                    if (containerToCountIn == ContainerType.Inventory) {
                        _currenctCountingPosition = new int[2] {j, i};
                    }

                    MoveMouse(containerToCountIn, j, i);
                    Thread.Sleep(10);
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYDOWN, 0);
                    keybd_event(C, 0, KEYEVENTF_KEYDOWN, 0);
                    keybd_event(C, 0, KEYEVENTF_KEYUP, 0);
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYUP, 0);
                    Thread.Sleep(10);
                }
            }
        }

        private void ClipboardChanged(ClipboardFormat format, object data) {
            string clipboardText = Clipboard.GetText(TextDataFormat.Text);
            string[] splittedString = clipboardText.Split('\n');
            if (splittedString[0].Contains("Currency")) {
                string currency = splittedString[1].Replace("\r", "");
                string[] stackSizeArray = splittedString[3].Split(' ');
                int stack = int.Parse(stackSizeArray[stackSizeArray.Length - 1].Split('/')[0].Replace(".", ""));

                if (_containerToCountCurrenciesIn == ContainerType.Trade) {
                    Currency currencyEntry = _currencyList.Find(entry => (entry.Name == currency));
                    if (currencyEntry != null) {
                        currencyEntry.Amount += stack;
                    } else {
                        Currency newCurrency = new Currency();
                        newCurrency.Name = currency;
                        newCurrency.Amount = stack;
                        _currencyList.Add(newCurrency);
                    }

                    StateHandler.Instance.CountedCurrencies = _currencyList;

                } else if (_containerToCountCurrenciesIn == ContainerType.Inventory) {
                    Item item = new Item(currency, 1, 1);
                    item.IsCurrency = true;
                    item.InInventory = true;
                    item.CurrentPosition = _currenctCountingPosition;
                    item.StackSize = int.Parse(splittedString[3].Split(' ')[2].Split('/')[0]);

                    StateHandler.Instance.ItemsInCountingContainer.Add(item);
                }
            } else {
                if (_containerToCountCurrenciesIn == ContainerType.Trade) {
                    Console.WriteLine("TODO - Send message about that you only accept currencies");
                }
            }
        }
    }
}
