﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trading.Objects;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Trading {

    class UtilService {
        public Dictionary<string, CurrencyProperties> CurrencyProperties { get; private set; }

        public Dictionary<string, int> CurrencyValues { get; private set; }

        public UtilService() {
            CurrencyProperties = new Dictionary<string, CurrencyProperties>();
            CurrencyValues = new Dictionary<string, int>();

            CurrencyProperties.Add("Chaos Orb", new CurrencyProperties("Chaos Orb", 10));
            CurrencyProperties.Add("Exalted Orb", new CurrencyProperties("Exalted Orb", 10));

            // TODO - Get prices from external site. Maybe PoE ninja?
            CurrencyValues.Add("Chaos", 1);
            CurrencyValues.Add("Exalted", 23);
        }

        public double GetValue(Currency currency) {
            Console.WriteLine("Getting value on: " + currency.Name);
            Console.WriteLine("With amount: " + currency.Amount);
            switch (currency.Name) {
                case "Chaos Orb": case "chaos":
                    return currency.Amount * CurrencyValues["Chaos"];
                case "Exalted Orb": case "exa": case "exalted":
                    return currency.Amount * CurrencyValues["Exalted"];
                default:
                    return 0;
            }
        }

        public string MapCurrencyName(string currencyName) {
            switch(currencyName) {
                case "Chaos Orb": case "chaos":
                    return "Chaos Orb";
                case "Exalted Orb": case "exalted":
                    return "Exalted Orb";
                default:
                    return "";
            }
        }
    }

    class SplitAmounrProperties {
        public int AmountToSplit { get; private set; }
        public ContainerType ContainerToTransferTo { get; private set; }
        public int[] Position { get; private set; }

        public SplitAmounrProperties(int splitAmount, ContainerType containerToTransferTo, int[] position) {
            AmountToSplit = splitAmount;
            ContainerToTransferTo = containerToTransferTo;
            Position = position;
        }
    }

    public static class CloneUtil {
        public static T DeepClone<T>(this T obj) {
            using (var ms = new MemoryStream()) {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
    }
}
