﻿using System;

namespace Trading.Objects {
    [Serializable]
    class Currency {
        public string Name { get; set; }
        public double Amount { get; set; }
    }

    class ListedCurrency {
        public Currency Currency { get; set; }
        public Currency Price { get; set; }
    }

    class CurrencyProperties {
        public string Name { get; private set; }
        public int MaxStackSize { get; private set; }

        public CurrencyProperties(string name, int maxStackSize) {
            Name = name;
            MaxStackSize = maxStackSize;
        }
    }
}
