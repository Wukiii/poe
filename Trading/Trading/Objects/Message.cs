﻿using System;
using System.Globalization;

namespace Trading.Handlers {
    class Message {
        public long TimeStamp { get; private set; }
        public DateTime Date { get; private set; }
        public MessageType TypeOFMessage { get; private set; }
        public string AccountName { get; private set; }
        public string CharacterName { get; private set; }
        public string ShortGuildName { get; private set; }
        public string TimeInMinutes { get; private set; }
        public string MessageText { get; private set; }
        public bool IsJoinedArea { get; private set; }

        public bool ignoreMessage;

        public Message(string logMessage) {
            string[] preMessageWords = logMessage.Split(' ');
            AccountName = ""; // TODO - Laves om, hvis der kan laves et lookup til account name
            ignoreMessage = true;

            if (preMessageWords.Length > 7) {
                switch (preMessageWords[7][0]) {
                    case '$':
                        //TypeOFMessage = MessageType.Trade;
                        //MessageText = logMessage.Split(new string[] { ": " }, 2, StringSplitOptions.None)[1];
                        //SaveInformation(preMessageWords);
                        break;
                    case '@':
                        if (preMessageWords[7] == "@From") {
                            TypeOFMessage = MessageType.Whisper;
                            ignoreMessage = false;
                            MessageText = logMessage.Split(new string[] { ": " }, 2, StringSplitOptions.None)[1];
                            SaveInformation(preMessageWords);
                        }
                        break;
                    case ':':
                        if (preMessageWords.Length > 10 && 
                            (preMessageWords[10].Equals("joined") || preMessageWords[10].Equals("left"))) 
                        {
                            TypeOFMessage = MessageType.Area;
                            ignoreMessage = false;
                            SaveInformation(preMessageWords);
                            break;
                        }
                        if (preMessageWords[8].Equals("Trade") && preMessageWords[9].Equals("cancelled.")) {
                            StateHandler.Instance.CurrentTradingPartyEntry.RejectedTrade = true;
                            break;
                        }

                        if (preMessageWords[8].Equals("Trade") && preMessageWords[9].Equals("accepted.")) {
                            StateHandler.Instance.FinishTrade();
                            break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void UpdateTime() {
            TimeSpan passedTime = DateTime.Now - Date;
            TimeInMinutes = passedTime.Minutes > 0 ? passedTime.Minutes + " min" : passedTime.Seconds + " sec";
        }

        private void SaveInformation(string[] preMessageWords) {
            Date = DateTime.ParseExact(preMessageWords[0] + " " + preMessageWords[1],
                                            "yyyy/MM/dd HH:mm:ss",
                                            CultureInfo.InvariantCulture);

            TimeStamp = (long)Date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            TimeSpan passedTime = DateTime.Now - Date;
            TimeInMinutes = passedTime.Minutes > 0 ? passedTime.Minutes + " min" : passedTime.Seconds + " sec";

            if (TypeOFMessage == MessageType.Area) {
                CharacterName = preMessageWords[8];
                if (preMessageWords[10].Equals("joined")) {
                    IsJoinedArea = true;
                } else {
                    IsJoinedArea = false;
                }

                return;
            }

            if (TypeOFMessage == MessageType.Whisper) {
                if (preMessageWords[8][0] == '<') {
                    ShortGuildName = preMessageWords[8];
                    CharacterName = preMessageWords[9].Substring(0, preMessageWords[9].Length - 1);
                } else {
                    CharacterName = preMessageWords[8].Substring(0, preMessageWords[8].Length - 1);
                }

                return;
            } 
            //else {
            //    if (preMessageWords[7][1] == '<') {
            //        ShortGuildName = preMessageWords[7].Substring(1);
            //        CharacterName = preMessageWords[8].Substring(0, preMessageWords[8].Length - 1);
            //    } else {
            //        CharacterName = preMessageWords[7].Substring(1, preMessageWords[7].Length - 2);
            //    }

            //    // Getting account name
            //    //using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(DataService.GetAccountNameAsync(CharacterName).Result))) {
            //    //    // Deserialization from JSON  
            //    //    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Account));
            //    //    Account accounts = (Account)deserializer.ReadObject(ms);
            //    //    if (accounts.accountName != null) {
            //    //        Console.WriteLine("Name: " + accounts.accountName);
            //    //        AccountName = accounts.accountName;
            //    //    }
            //    //    Console.WriteLine("Error: " + accounts.error);
            //    //}
            //}
        }
    }
}
