﻿using System;
using System.Collections.Generic;
using Trading.Handlers;

namespace Trading.Objects {
    class PartyEntry {
        public Message Message { get; private set; }
        public Item ItemToSell { get; private set; }
        public List<Item> CurrenciesToSell { get; private set; }
        public string CurrencyAmountTradeMessage { get; private set; }
        public string CurrencyPriceTradeMessage { get; private set; }
        public bool RejectedTrade { get; set; }
        public bool TradeDone { get; set; }
        public bool IsInTheArea { get; set; }


        public PartyEntry(Message message, Item itemToSell) {
            Message = message;
            ItemToSell = itemToSell;
            TradeDone = false;
            RejectedTrade = false;
        }

        public PartyEntry(Message message, List<Item> currenciesToSell, string currencyAmountTradeMessage, string currencyPriceTradeMessage) {
            Message = message;
            TradeDone = false;
            RejectedTrade = false;
            CurrenciesToSell = currenciesToSell;
            CurrencyAmountTradeMessage = currencyAmountTradeMessage;
            CurrencyPriceTradeMessage = currencyPriceTradeMessage;
        }
    }
}
