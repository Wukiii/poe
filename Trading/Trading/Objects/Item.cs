﻿using System;

namespace Trading.Objects {
    [Serializable]
    class Item {
        public string Name { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int StackSize { get; set; }
        public int StashTab { get; set; }
        public bool InInventory { get; set; }
        public bool IsCurrency { get; set; }
        public int[] CurrentPosition { get; set; }
        public Currency Price { get; set; }

        public Item(string name, int width, int height) {
            Name = name;
            Width = width;
            Height = height;
            StackSize = 1;
            StashTab = 0;
            InInventory = false;
            IsCurrency = false;
            CurrentPosition = new int[2];
            Price = new Currency();
        }
    }
}
