import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { LeagueDataComponent } from './components/league/league.component';
import { LadderDataComponent } from './components/ladder/ladder.component';
import { ColorThemeComponent } from './components/colortheme/colortheme.component';



export const sharedConfig: NgModule = {
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        LeagueDataComponent,
        LadderDataComponent,
        ColorThemeComponent
    ],
    imports: [
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'league', component: LeagueDataComponent },
            { path: 'ladders/:leagueName', component: LadderDataComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
};
