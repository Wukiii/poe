﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'league',
    templateUrl: './league.component.html'
})
export class LeagueDataComponent {
    public leagues: League[];

    constructor(http: Http, @Inject('ORIGIN_URL') originUrl: string) {
        http.get(originUrl + '/api/SQLData/Leagues').subscribe(result => {
            this.leagues = result.json() as League[];
        });
    }
}

interface League {
    leagueName: string;
}
