﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'colortheme',
    templateUrl: './colortheme.component.html',
    styleUrls: ['./colortheme.component.css']
})
export class ColorThemeComponent {
    currentTheme: string = "light";

    setColorTheme(theme: string) {
        let body = document.getElementsByTagName('body')[0];
        body.classList.remove(this.currentTheme);   //remove the class
        body.classList.add(theme);   //add the class    
        this.currentTheme = theme;
    }
}