﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from '../../services/pager.service';

@Component({
    selector: 'ladders',
    templateUrl: './ladder.component.html',
    styleUrls: ['../../../../Styles.css', 'ladder.component.css']
})
export class LadderDataComponent {
        // array of all items to be paged
    public entries: Ladder[];
    private leagueName;
    private searchValue: string = "";
    private highlightRank: string[] = [];
    private errorMessage: string;
    private multipleResult: Ladder[];
    private searchResults: Ladder[] = [];
    private entryPerPage = 20;

    constructor(private http: Http, @Inject('ORIGIN_URL') private originUrl: string, private route: ActivatedRoute, private pagerService: PagerService) {     
        route.params.subscribe(params => { this.leagueName = params['leagueName'] })
        http.get(originUrl + '/api/SQLData/Ladder?leagueName=' + this.leagueName).subscribe(result => {
            this.entries = result.json() as Ladder[];
            this.setPage(1);
        });
    }

    search() {
        this.highlightRank = [];
        this.searchResults = [];
        this.errorMessage = undefined;
        if (this.searchValue != "") {
            this.http.get(this.originUrl + '/api/SQLData/Ladder?leagueName=' + this.leagueName + '&input=' + this.searchValue).subscribe(result => {
                this.searchResults = result.json() as Ladder[];
             
                if (this.searchResults.length == 0) {
                    this.errorMessage = "No results found";
                } else {
                    for (let entry of this.searchResults) {
                        this.highlightRank.push(entry.rank.toString());
                    }

                    if (this.searchResults.length == 1) {
                        this.setPage(Math.ceil(this.searchResults[0].rank / this.entryPerPage));
                    }
                }
            })
        }       
    }
    
    // pager object
    pager: any = {};

    // paged items
    pagedItems: Ladder[];

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.entries.length, page);

        // get current page of items
        this.pagedItems = this.entries.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    setPageByRank(rank: number) {
        this.setPage(Math.ceil(rank / this.entryPerPage))
    }
}

interface Ladder {
    accountChallenges: number;
    accountName: string;
    accountTwitchName: string;
    characterClass: string;
    characterExperience: number;
    characterId: string;
    characterName: string;
    characterLevel: number;
    dead: boolean;
    leagueName: string;
    online: boolean;
    rank: number;
}
