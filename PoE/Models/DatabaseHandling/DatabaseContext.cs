﻿using Microsoft.EntityFrameworkCore;
using PoE.Models;

namespace PoE.Models.DatabaseHandling
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        public virtual DbSet<Leagues> Leagues { get; set; }
        public virtual DbSet<Ladders> Ladders { get; set; }
    }
}
