﻿using System.Linq;

namespace PoE.Models.DatabaseHandling
{
    public class Queries
    {
        private DatabaseContext dbContext;

        public Queries(DatabaseContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IQueryable<Leagues> GetLeagueList()
        {
            return from leagues in dbContext.Leagues select leagues;
        }

        public IQueryable<Ladders> GetLadder(string leagueName)
        {
            return from ladders in dbContext.Ladders
                   where ladders.League.Equals(leagueName)
                   select ladders;
        }

        public IQueryable<Ladders> GetSearchLadder(string leagueName, string input)
        {
            return from ladders in dbContext.Ladders
                   where ladders.League.Equals(leagueName) && 
                         (ladders.CharacterName.Equals(input) || ladders.AccountName.Equals(input))
                   select ladders;
        }
    }
}
