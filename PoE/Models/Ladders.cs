﻿using System.ComponentModel.DataAnnotations;

namespace PoE.Models
{
    public class Ladders
    {
        public int AccountChallenges { get; set; }
        public string AccountName { get; set; }
        public string AccountTwitchName { get; set; }
        public string CharacterClass { get; set; }
        public long CharacterExperience { get; set; }

        [Key]
        public string CharacterId { get; set; }
        public string CharacterName { get; set; }
        public int CharacterLevel { get; set; }
        public bool Dead { get; set; }
        public string League { get; set; }
        public bool Online { get; set; }
        public int Rank { get; set; }
    }
}
