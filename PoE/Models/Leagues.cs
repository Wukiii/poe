﻿using System.ComponentModel.DataAnnotations;
namespace PoE.Models
{
    public class Leagues
    {
        [Key]
        public string LeagueName { get; set; }
    }
}
