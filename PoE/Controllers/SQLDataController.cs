﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PoE.Models;
using PoE.Models.DatabaseHandling;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PoE.Controllers
{
    [Route("api/[controller]")]
    public class SQLDataController : Controller
    {
        private Queries queryHandler;
        public SQLDataController(DatabaseContext dbContext)
        {
            queryHandler = new Queries(dbContext);
        }

        [HttpGet("[action]")]
        public IEnumerable<Leagues> Leagues()
        {
            var leagueQuery = queryHandler.GetLeagueList();

            return leagueQuery;
        }

        [HttpGet("{leagueName}")]
        public IEnumerable<Ladders> GetLadder([FromQuery]string leagueName, [FromQuery]string input)
        {            
            if (input == null)
            {
                var leagueQuery = queryHandler.GetLadder(leagueName);
                return leagueQuery;
            }
            else
            {
                var leagueQuery = queryHandler.GetSearchLadder(leagueName, input);
                return leagueQuery;
            }
        }

        //[HttpGet("{leagueName, input}")]
        //public Ladders GetSearchLadder([FromQuery]string leagueName, [FromQuery]string input)
        //{
        //    var leagueQuery = queryHandler.GetSearchLadder(leagueName, input);
        //    return leagueQuery.First();
        //}
    }
}
